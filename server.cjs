const fs = require("fs");
const { join } = require('path');
const express = require('express');
const cors = require("cors");

const app = express();
app.use(express.json());
app.use(cors())

const port = 7999;

const staticPath = "static";
const dataPath = "data";

const tasksFile = "tasks.json";

function buildDataPath(relative) {
    return join(__dirname, dataPath, relative);
}

// Serve static assets.
app.use('/static', express.static(join(__dirname, staticPath)));

app.get('/api/v1/tasks', (req, res) => {
    res.sendFile(buildDataPath(tasksFile));
});

app.post("/api/v1/tasks", (req, res) => {
    const bodyStr = JSON.stringify(req.body, null, 2)
    fs.writeFileSync(buildDataPath(tasksFile), bodyStr)
})

app.listen(port, () => {
    console.log("Listening on "+port);
    console.log("Ctrl+C to stop.")
});

