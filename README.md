# tasker-server

Minimal backend that serves and saves a list of tasks.

## Project Setup

```sh
npm install
```

## Run server

```sh
npm run server
```
